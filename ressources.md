---
title: "Ressources"
order: 3
in_menu: false
---
# Livres



# Vidéos

<iframe width="1200" height="300" src="https://www.youtube.com/embed/lRHO5GfKgFY" title="Manifeste : Pour un statut politique du producteur (Réseau Salariat)" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe> 

<iframe width="1200" height="300" src="https://www.youtube.com/embed/YcALBLzwEp0" title="La cotisation sociale, c&#39;est ULTRA PUISSANT !!!" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

## *Espoir Commun*

<iframe width="1200" height="300" src="https://www.youtube.com/embed/xKwnJXv2vf8" title="Et Pour Quelques Annuités de Plus - L&#39;enjeu des retraites" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

<iframe width="1200" height="300" src="https://www.youtube.com/embed/tkNztevYS1I" title="Retraité, Délivré - Vers le salaire à vie" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

<iframe width="1200" height="300" src="https://www.youtube.com/embed/WL9YqzqD2Xk" title="Locataires de tous les pays, unissez-vous ! - Marché de l&#39;immobilier ou droit au logement ?" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

## *Mes chers contemporains*

<iframe width="1200" height="300" src="https://www.youtube.com/embed/uhg0SUYOXjw" title="Le Salaire à Vie (Bernard Friot)" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

# Tracts & Affiches

![Avenir heureux]({% link images/avenir-heureux-hor.png %})

![Pension complète]({% link images/Pension-complète.png %}) 