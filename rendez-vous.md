---
title: "Rendez-vous"
order: 2
in_menu: true
---
<iframe width="1200" height="600" src="https://nuage.reseau-salariat.info/apps/calendar/embed/9eP29QNHGHb8NexH"></iframe> 

# À venir
## Lundi 20 mai 2024 à 17H
### Présentation du livre *L'imposture du travail*

**Désandrocentrer le travail pour l'émanciper**

*En présence de l'autrice Maud Simonet*

Local de L'Offensive\
41 rue de Valmy, 59000 Lille

Plus d'informations sur [Démosphère](https://lille.demosphere.net/rv/9691)

---

# Passés
## Mercredi 15 mai 2024 à 20H
### Conférence gesticulée *Impertinente du spectacle - "T'as pas la gueule de l'emploi !"*

**Quand une chômeuse part explorer le prétendu trou de la sécu, et découvre, avec humour, la lutte des classes**

*Conférence gesticulée d'Hélène Vitorge*

Café Le Polder\
250 rue Roger Salengro, 59260 Hellemmes

Plus d'informations sur [Démosphère](https://lille.demosphere.net/rv/9687) 