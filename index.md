---
title: "Accueil"
order: 1
in_menu: true
---
![Réseau Salariat]({% link images/logo-site-nord.png %})

Réseau Salariat est une association d’éducation populaire qui réunit des travailleuses et des travailleurs de tous horizons : salarié.e.s d’associations, d’entreprises et de la fonction publique, syndicalistes, mais encore retraité.e.s, entrepreneuses et entrepreneurs, chômeuses et chômeurs, parents et étudiant.e.s.

Notre objectif est de prolonger, diffuser une pensée révolutionnaire orientée vers l’appropriation collective des moyens de production (aussi bien industriel que sanitaire, culturel, éducatif...) et l’octroi à toutes et à tous d’un salaire à vie.

Contre le capitalisme, le marché du travail et la propriété à but lucratif (que nous distinguons de la propriété d’usage), nous voulons notamment continuer et étendre les expériences révolutionnaires de la cotisation sociale et du salaire à vie. La cotisation finance en effet des pensions et des soins de santé libérés de la propriété lucrative et montre qu’il n’y a nul besoin d’accumulation financière pour financer l’investissement. Le salaire à la qualification du secteur privé, le grade de la fonction publique, la pension des retraités montrent quant à eux que l’on travaille mieux, et pour faire des choses plus utiles, quand on est libéré du marché du travail. Nous pouvons nous appuyer sur ces anticipations pour poser au coeur des droits politiques notre capacité, individuelle et collective, de créer la valeur économique sans employeurs ni prêteurs.

Réseau-Salariat s'inspire de l'expérience massive et de long terme d'un déjà-là révolutionnaire. En effet, des pratiques collectives de travail et de production alternatives au système capitaliste sont pratiquées au quotidien par les fonctionnaires, les parents, les retraité.e.s, les chômeur.e.s... Des pratiques qui sont systématiquement soit passées sous silence, soit dévalorisée par l’idéologie capitaliste. Réseau salariat se veut un espace de partage et de diffusion de ces pratiques révolutionnaires et de leur transformation sous la forme de savoirs collectifs et militants.

Et parce que ces savoirs doivent être accessibles à tou.te.s, nous utilisons les outils de l'éducation populaire pour les transmettre.

<iframe width="1200" height="600" src="https://www.youtube.com/embed/lRHO5GfKgFY" title="Manifeste : Pour un statut politique du producteur (Réseau Salariat)" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe> 